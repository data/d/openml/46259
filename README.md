# OpenML dataset: Electricity-hourly

https://www.openml.org/d/46259

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Electricity Load Diagrams between 2011 and 2014, resampled hourly.

From original source:
-----
Data set has no missing values.
Values are in kW of each 15 min. To convert values in kWh values must be divided by 4.
Each column represent one client. Some clients were created after 2011. In these cases consumption were considered zero.
All time labels report to Portuguese hour. However all days present 96 measures (24*4). Every year in March time change day (which has only 23 hours) the values between 1:00 am and 2:00 am are zero for all points. Every year in October time change day (which has 25 hours) the values between 1:00 am and 2:00 am aggregate the consumption of two hours.

Data set were saved as txt using csv format, using semi colon (;).
First column present date and time as a string with the following format 'yyyy-mm-dd hh:mm:ss'
Other columns present float values with consumption in kW
-----

There are 319 columns:

id_series: The id of the time series.

date: The date of the time series in the format "%Y-%m-%d %H:%M:%S".

time_step: The time step on the time series.

value_X (X from 0 to 315): The values of the time series, which will be used for the forecasting task.

Preprocessing:

1 - Renamed first column to 'date' and standardize the format to "%Y-%m-%d %H:%M:%S".

2 - Dropped rows with the year <= 2011

As the original description tell us, some clients were only registered after 2011, so before 2011 most of the entries are 0.

3 - Dropped columns where the % of 0 entries were greater than 10%.

This effectivelly drop 54 columns (15% of columns), namely: 'MT_012','MT_015','MT_030','MT_032','MT_033','MT_039','MT_041','MT_066','MT_092','MT_106','MT_107','MT_108','MT_109','MT_110','MT_111','MT_112','MT_113','MT_115','MT_116','MT_117','MT_120','MT_121','MT_122','MT_127','MT_130','MT_131','MT_132','MT_133','MT_134','MT_144','MT_152','MT_160','MT_165','MT_167','MT_170','MT_177','MT_178','MT_179','MT_181','MT_184','MT_185','MT_186','MT_223','MT_224','MT_255','MT_289','MT_305','MT_308','MT_322','MT_332','MT_337','MT_347','MT_348','MT_370'.

4 - We resampled the dataset by summing the values of each 'MT_X' column hourly (```resample('h', on='date').sum()```)

5 - Renamed columns [1:] to 'value_X' with X from 0 to 315.

6 - Created 'id_series' with value 0. There is only one multivariate time series.

7 - Ensured that there are no missing dates and that the frequency of the time_series is 1 hour.

8 - Created 'time_step' column from 'date' and 'id_series' with increasing values from 0 to the size of the time series.

9 - Casted 'date' to str, 'time_step' to int, 'value_X' to float, and defined 'id_series' as 'category'.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46259) of an [OpenML dataset](https://www.openml.org/d/46259). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46259/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46259/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46259/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

